<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateFeedBacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_feed_backs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id');
            $table->text('support_id');
            $table->text('star')->nullable();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->text('received')->nullable();
            $table->text('secure');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_feed_backs');
    }
}
