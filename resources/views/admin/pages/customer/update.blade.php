@extends('admin.base')
            @section('content')

                <div class="container-fluid right_color">
                    <div class="page-main-header">

                        <form action="{{route('admin.customer.profile')}}" method="post">

                            @csrf
                        <div style="padding: 60px 200px 60px 300px;">
                            <div class="section-header">
                                <h2>Create Customer</h2>
                            </div>
                            <div class="section-body">
                                <label>User Id</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="hidden"  name="id" value="{{$customer->id}}" placeholder="User id">
                                        <input class="form-control" type="text"  name="user_id" value="{{$customer->user_id}}" placeholder="User id">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    </div>
                                </div>
                                <label>First Name</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="text"  name="fname" value="{{$customer->fname}}"  placeholder="First name">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    </div>
                                </div>
                                <label>Last Name</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="text"  value="{{$customer->lname}}"  name="lname" placeholder="Last name">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    </div>
                                </div>
                                <label>Address</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="text"  value="{{$customer->address}}"  name="address" placeholder="Address">
                                        <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    </div>
                                </div>

                                <label>Phone</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="tel"  value="{{$customer->phone}}"  name="phone" placeholder="Phone">
                                        <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                    </div>
                                </div>
                                <label>Alternate Phone</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="text"  value="{{$customer->alternate_phone}}"  name="alternate_phone" placeholder="Alternate phone">
                                        <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                    </div>
                                </div>



                                <label>Email</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="email"  value="{{$customer->email}}"  name="email" placeholder="Enter Email">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                </div>

                                <label>plan_value</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="text"  value="{{$customer->plan_value}}"  name="plan_value" placeholder="plan Value">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                </div>

                                <label>plan</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" type="text" value="{{$customer->plan}}" name="plan" placeholder="plan">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    </div>
                                </div>

                               \


                                <button type="submit" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">Submit</button>
                                <button type="button" class="btn btn-danger btn-outline float-button-light waves-effect waves-button waves-float waves-light">Reset</button>

                            </div>
                        </div>
            <!------ Include the above in your HEAD tag ---------->
                        </form>


        </div>
    </div>


    @endsection
