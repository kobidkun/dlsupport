<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateReferalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_referals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id');
            $table->text('ref_name');
            $table->text('ref_email')->nullable();
            $table->text('ref_phone');
            $table->text('ref_address')->nullable();
            $table->text('ref_pin')->nullable();
            $table->text('is_converted')->nullable();
            $table->text('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_referals');
    }
}
