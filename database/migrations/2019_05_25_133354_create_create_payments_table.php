<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('fname');
            $table->text('lname');
            $table->text('customer_id');
            $table->text('email');
            $table->text('phone');
            $table->text('user_id');
            $table->text('plan');
            $table->text('amount');
            $table->text('status')->nullable();
            $table->text('txtid');
            $table->text('methods')->nullable();
            $table->text('gateway_dump')->nullable();
            $table->text('net_amount_debit')->nullable();
            $table->text('card_type')->nullable();
            $table->text('issuing_bank')->nullable();
            $table->text('name_on_card')->nullable();
            $table->text('cardnum')->nullable();
            $table->text('bank_ref_num')->nullable();
            $table->text('mode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_payments');
    }
}
