@extends('front.web.base')

@section('content')


    <div class="jumbotron text-xs-center">
        <h1 class="display-3">Hi {{$cust->fname}}</h1>
        <p class="lead"><strong>Please Reset your password</strong> </p>
        <hr>
        <p>
            Having trouble? <a href="">{{$cust->id}}</a>
        </p>
        <p class="lead">


        <form action="{{route('password.reset.update')}}" method="post">

            @csrf

            <input type="hidden" value="{{$cust->id}}" name="id">

            <input type="password" required placeholder="Password" name="password">


            <button type="submit">Save</button>

        </form>





        </p>
    </div>




@stop