@extends('admin.base')
@section('content')

    <div class="container-fluid right_color">
        <div class="page-main-header">



            <style>
                #customers {
                    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                    border-collapse: collapse;
                    width: 100%;
                }

                #customers td, #customers th {
                    border: 1px solid #ddd;
                    padding: 8px;
                }

                #customers tr:nth-child(even){background-color: #f2f2f2;}

                #customers tr:hover {background-color: #ddd;}

                #customers th {
                    padding-top: 12px;
                    padding-bottom: 12px;
                    text-align: left;
                    background-color: #4CAF50;
                    color: white;
                }
            </style>



            <table id="customers">
                <tr>

                    <th>Sl.No</th>

                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Alfreds Futterkiste</td>
                    <td>MariaAnders@g.com</td>
                    <td><input type="submit"  class="btn btn-success" value="Edit"><input type="submit"  class="btn btn-danger" value="Delete"><input type="submit"  class="btn btn-warning" value="View Details"></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Berglunds snabbköp</td>
                    <td>ChristinaBerglund@g.com</td>
                    <td><input type="submit"  class="btn btn-success" value="Edit"><input type="submit"  class="btn btn-danger" value="Delete"><input type="submit"  class="btn btn-warning" value="View Details"></td>

                </tr>

            </table>




        </div>
    </div>
@endsection
