@extends('admin.base')
@section('content')

    <div class="container-fluid right_color">
        <div class="page-main-header">
            <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10">
                <div class="section-header">
                    <h2>Create Staff</h2>
                </div>
                <div class="section-body">
                    <label>Name</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Name">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>
                    <label>Email</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="email" value="" placeholder="email">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                    <label>Password</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Details">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        </div>
                    </div>










                    <button type="button" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">Submit</button>
                    <button type="button" class="btn btn-danger btn-outline float-button-light waves-effect waves-button waves-float waves-light">Reset</button>

                </div>
            </div>

            <!------ Include the above in your HEAD tag ---------->


        </div>
    </div>


@endsection
