<?php

namespace App\Http\Controllers\PublicController;

use App\Model\Payment\CreatePayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tzsk\Payu\Facade\Payment;

class ManagePayment extends Controller
{
    public function PayNow(){

        $attributes = [
            'txnid' => strtoupper(str_random(8)), # Transaction ID.
            'amount' => rand(100, 999), # Amount to be charged.
            'productinfo' => "Product Information",
            'firstname' => "John", # Payee Name.
            'email' => "john@doe.com", # Payee Email Address.
            'phone' => "9876543210", # Payee Phone Number.
        ];

        return Payment::make($attributes, function ($then) {
          //  $then->redirectTo('payment/status');
            # OR...
            $then->redirectRoute('payment.status');
            # OR...
          //  $then->redirectAction('PaymentController@status');
        });

    }

    public function CapturePaymentSuccessWeb(Request $request){

        $payment = Payment::capture();

       // dd($payment);

        if ($payment->isCaptured()  === true ){


            $findtxt = CreatePayment::where('txtid',$payment->txtid)->first();

            $findtxt->status = 'success';
            $findtxt->data = $payment->data;
            $findtxt->issuing_bank = $payment->issuing_bank;
            $findtxt->card_type = $payment->card_type;
            $findtxt->net_amount_debit = $payment->net_amount_debit;
            $findtxt->name_on_card = $payment->name_on_card;
            $findtxt->cardnum = $payment->cardnum;
            $findtxt->name_on_card = $payment->name_on_card;
            $findtxt->cardnum = $payment->cardnum;
            $findtxt->bank_ref_num = $payment->bank_ref_num;
            $findtxt->mode = $payment->mode;

            $findtxt->save();

           // dd($payment);




            return view('front.web.pages.success');
        } else {


            return view('front.web.pages.failure');

        }




    }


    public function CapturePaymentSuccessMobile(Request $request){

        $payment = Payment::capture();

       // dd($payment);

        if ($payment->isCaptured()  === true ){


            $findtxt = CreatePayment::where('txtid',$payment->txtid)->first();

            $findtxt->status = 'success';
            $findtxt->data = $payment->data;
            $findtxt->issuing_bank = $payment->issuing_bank;
            $findtxt->card_type = $payment->card_type;
            $findtxt->net_amount_debit = $payment->net_amount_debit;
            $findtxt->name_on_card = $payment->name_on_card;
            $findtxt->cardnum = $payment->cardnum;
            $findtxt->name_on_card = $payment->name_on_card;
            $findtxt->cardnum = $payment->cardnum;
            $findtxt->bank_ref_num = $payment->bank_ref_num;
            $findtxt->mode = $payment->mode;

            $findtxt->save();




            return response()->json([
                'status' => 'success',
                'data' => $payment
            ]);
        } else {


            return response()->json([
                'status' => 'failure',
                'data' => $payment
            ]);

        }




    }



    public function CreateWebPayment($slug){

        $payment = CreatePayment::where('txtid', $slug)->first();

        $attributes = [
            'txnid' => $payment->txtid, # Transaction ID.
            'amount' => $payment->amount, # Amount to be charged.
            'productinfo' => $payment->plan,
            'firstname' => $payment->fname, # Payee Name.
            'email' => $payment->email, # Payee Email Address.
            'phone' => $payment->phone, # Payee Phone Number.
        ];

        return Payment::make($attributes, function ($then) {
            //  $then->redirectTo('payment/status');
            # OR...





            $then->redirectRoute('pay.mob');
            # OR...
            //  $then->redirectAction('PaymentController@status');
        });


    }


    public function CreateMobPayment($slug){

        $payment = CreatePayment::where('txtid', $slug)->first();

        $attributes = [
            'txnid' => $payment->txtid, # Transaction ID.
            'amount' => $payment->amount, # Amount to be charged.
            'productinfo' => $payment->plan,
            'firstname' => $payment->fname, # Payee Name.
            'email' => $payment->email, # Payee Email Address.
            'phone' => $payment->phone, # Payee Phone Number.
        ];

        return Payment::make($attributes, function ($then) {
            //  $then->redirectTo('payment/status');
            # OR...





            $then->redirectRoute('pay.mob.status');
            # OR...
            //  $then->redirectAction('PaymentController@status');
        });


    }


}
