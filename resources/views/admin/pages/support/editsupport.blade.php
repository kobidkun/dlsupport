@extends('admin.base')
@section('content')

    <div class="container-fluid right_color">
        <div class="page-main-header">
            <div class="col-lg-6 col-md-6 col-sm-10 col-xs-10">
                <div class="section-header">
                    <h2>Create Support</h2>
                </div>
                <div class="section-body">
                    <label>Customer Id</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Customer id">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>
                    <label>Title</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Title">
                            <span class="input-group-addon"><i class="fa fa-file"></i></span>
                        </div>
                    </div>
                    <label>Details</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Details">
                            <span class="input-group-addon"><i class="fa fa-file"></i></span>
                        </div>
                    </div>
                    <label>Assigned to</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Assigned to">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>

                    <label>Status</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Status">
                            <span class="input-group-addon"><i class="fa fa-check"></i></span>
                        </div>
                    </div>
                    <label>Medium</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Medium">
                            <span class="input-group-addon"><i class="fa fa-file"></i></span>
                        </div>
                    </div>
                    <label>Closed By</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Closed By">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>


                    <label>Result</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" value="" placeholder="Result">
                            <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                        </div>
                    </div>




                    <button type="button" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">Submit</button>
                    <button type="button" class="btn btn-danger btn-outline float-button-light waves-effect waves-button waves-float waves-light">Reset</button>

                </div>
            </div>

            <!------ Include the above in your HEAD tag ---------->


        </div>
    </div>


@endsection
