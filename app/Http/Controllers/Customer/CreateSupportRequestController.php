<?php

namespace App\Http\Controllers\Customer;

use App\Model\Customer\Support\CreateSupport;
use App\Model\Customer\Support\CreateSupportToFiles;
use App\Model\Staff\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CreateSupportRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:customer-api');

    }



    public function CreateSupportRequest(Request $request){

        $this->AsignStaff();

        $user =  $request->user();

        $s = new CreateSupport();
        $s->customer_id = $user->id;
        $s->title = $request->title;
        $s->body = $request->body;
        $s->assigned_to =  $this->AsignStaff();
        $s->status = $request->status;
        $s->secure_id = $this->GenerateRandom();
        $s->medium = $request->medium;
        $s->closed_by = $request->closed_by;
        $s->result = $request->result;
        $s->alt_contact = $request->alt_contact;
        $s->updated_at = null;
        $s->save();

        return response()->json([
            'msg' => 'successful'
        ],200);
    }

    public function GenerateRandom(){
        $rand = rand(200000,1000000);
        $date = time();

        return $rand.$date;

    }

    public function GetSupportRequest(Request $request){

        $user =  $request->user();

        $a = CreateSupport::with('create_feed_backs')
            ->orderBy('created_at', 'DESC')
            ->where('customer_id', '=', $user->id)->get();

        return response()->json(
            $a
        ,200);

    }

    public function UploadImage(Request $request){


        $image = $request->image;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        \File::put(storage_path(). '/' . $imageName, base64_decode($image));


        $user =  $request->user();

        $a = new CreateSupportToFiles();
        $a->create_support_id = $request->create_support_id;
        $a->location = $imageName;
        $a->name = $request->name;
        $a->save();


        return response()->json(
            $a
            ,200);

    }




    public  function AsignStaff(){

        $staff = Staff::all()->random(1)->first();

        return $staff->id;

    }
}
