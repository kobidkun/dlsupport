<?php

namespace App\Model\Payment;

use Illuminate\Database\Eloquent\Model;

class CreatePayment extends Model
{
    public function create_customers()
    {
        return $this->belongsTo('App\Model\Customer\Customer','customer_id','id');
    }
}
