<?php

namespace App\Http\Controllers\Customer;

use App\Model\Customer\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer-api');

    }



    public function ReturnProfile(Request $request){
        $user =  $request->user();


        return response()->json([
            'msg' => 'success',
            'data' => $user
        ],200);


    }

    public function PasswordChange(Request $request){
        $user =  $request->user();

        $fiduse = Customer::find($user->id);

        $fiduse->password = bcrypt($request->password);

        $fiduse->save();

        return response()->json(true,200);








    }
}
