<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateSupportToRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_support_to_replies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id')->nullable();
            $table->text('staff_id')->nullable();
            $table->text('create_support_id')->nullable();
            $table->text('title')->nullable();
            $table->text('body')->nullable();
            $table->enum('replied_by', ['customer', 'admin','staff']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_support_to_replies');
    }
}
