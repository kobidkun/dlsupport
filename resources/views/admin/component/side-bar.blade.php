<!-- start Left Menu-->
<div class="col-md-3 left_color">
    <div class="left_color scroll-view">
        <div class="navbar nav_title">
            <a href="/" class="medium-logo">
                <img src="https://www.dreamlink.in/img/logo2.png" alt="medium-logo">
            </a>
            <a href="/" class="small-logo">
                <img src="https://www.dreamlink.in/img/logo2.png" alt="small-logo">
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li><a class="waves-effect waves-light" href="{{route('admin.dashboard.create')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a class="waves-effect waves-light"><i class="fa fa-desktop"></i>Create Customer <span
                                class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li> <a class="waves-effect waves-light" href="{{route('admin.customer')}}">All Customer</a></li>
                            <li> <a class="waves-effect waves-light" href="{{route('admin.customer.create')}}">Add Customer</a></li>

                        </ul>
                    </li>
                    {{--<li><a class="waves-effect waves-light"><i class="fa fa-cube"></i>Create Staff <span
                                class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li> <a class="waves-effect waves-light" href="{{route('admin.customer')}}">View Staff</a></li>
                            <li> <a class="waves-effect waves-light" href="{{route('admin.customer')}}">Add Staff</a></li>

                        </ul>
                    </li>--}}
                    <li><a  class="waves-effect waves-light"><i class="fa fa-edit"></i> Create Support<span class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="{{route('admin.support')}}">All Support</a></li>
                            <li><a class="waves-effect waves-light" href="{{route('admin.support.create')}}">Add Support</a></li>

                        </ul>
                    </li>


                    {{--<li><a  class="waves-effect waves-light"><i class="fa fa-edit"></i> Create Payment<span class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="{{route('admin.payment')}}">All Payment</a></li>
                            <li><a class="waves-effect waves-light" href="{{route('admin.payment.create')}}">Add Payment</a></li>

                        </ul>
                    </li>--}}
                    <li><a class="waves-effect waves-light"><i class="fa fa-table"></i> FeedBack Form <span class="fa fa-chevron-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a class="waves-effect waves-light" href="{{route('admin.feedback')}}">View Form</a></li>


                        </ul>
                    </li>
                    <li><a class="waves-effect waves-light" href="/export_page"><i class="fa fa-anchor"></i> Export Page <span class="fa fa-chevron-right"></span></a></li>

                </ul>
            </div>
            {{--<div class="menu_section">--}}
                {{--<h3>Live On</h3>--}}
                {{--<ul class="nav side-menu">--}}
                    {{--<li><a class="waves-effect waves-light"><i class="fa fa-anchor"></i> Icons <span--}}
                                {{--class="fa fa-chevron-right"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li><a class="waves-effect waves-light" href="font_awesome.html">Font awesome</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="material_icon.html">Material icon</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="themify_icon.html">Themify icon</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="flag_icon.html">Flag icon</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a class="waves-effect waves-light"><i class="fa fa-map-marker"></i> Maps <span--}}
                                {{--class="fa fa-chevron-right"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li><a class="waves-effect waves-light" href="map-google.html">Google Maps</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="map-vector.html">Vector Map</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a class="waves-effect waves-light"><i class="fa fa-user-circle-o"></i> User Pages <span--}}
                                {{--class="fa fa-chevron-right"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li><a class="waves-effect waves-light" href="user_login_v1.html">Login 1</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_login_v2.html">Login 2</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_register_v1.html">Register 1</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_register_v2.html">Register 2</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_forgot_password_v1.html">Forgot 1</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_forgot_password_v2.html">Forgot 2</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_lockscreen_v1.html">Lockscreen 1</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_lockscreen_v2.html">Lockscreen 2</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a class="waves-effect waves-light"><i class="fa fa-exclamation-circle"></i> Error Pages <span--}}
                                {{--class="fa fa-chevron-right"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li><a class="waves-effect waves-light" href="user_404.html">Error 404</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_400.html">Error 400</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_403.html">Error 403</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_405.html">Error 405</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_500.html">Error 500</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="user_503.html">Error 503</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a class="waves-effect waves-light"><i class="fa fa-balance-scale"></i> General Pages <span--}}
                                {{--class="fa fa-chevron-right"></span></a>--}}
                        {{--<ul class="nav child_menu">--}}
                            {{--<li><a class="waves-effect waves-light" href="faq.html">FAQ</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="userlist.html">User List</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="invoice.html">Invoice</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="blank.html">Blank</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="profile.html">Profile</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="gallery.html">Gallery</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="maintenance.html">Maintenance</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="draggable_grid.html">Draggable Grid</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="grids.html">Grids</a></li>--}}
                            {{--<li><a class="waves-effect waves-light" href="search_results.html">Search result</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li><a class="waves-effect waves-light" href="documentation.html"><i class="fa fa-cogs"></i> Documentation</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </a>
            <a class="toggle-fullscreen" data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="fa fa-arrows-alt" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="fa fa-lock" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="fa fa-power-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- End Left Menu -->
