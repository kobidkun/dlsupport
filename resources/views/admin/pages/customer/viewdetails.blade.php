@extends('admin.base')
@section('content')
    <div class="container-fluid right_color">
        <div class="page-main-header">

    <!-- Start Contain Section -->
    <div class="">


        <div class="contain-section">
            <div class="profile-background">

                <div class="profile-social">
                    <ul>
                        <li class="profile-name">
                            <h4>{{$customer->fname}} {{$customer->lname}}</h4>
                            <span><i class="fa fa-folder"></i> {{$customer->user_id}}</span>
                        </li>
                        <li class="profile-follow">
                            <h6>1,506</h6>
                            <span>Support</span>
                        </li>
                        <li class="profile-follow">
                            <h6>85</h6>
                            <span>Feedback</span>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-left-section">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="tab" href="#info" class="float-button-light">Information</a>
                            </li>

                            <li ><a href="{{route('admin.customer.profile.get',$customer->id)}}"
                                                  class="float-button-light">Edit</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div id="info" class="tab-pane fade in active">
                                <div class="section-body">
                                    <div class="profile-body">
                                        <h4 class="profile-title"><i class="fa fa-hand-o-right"></i> Personal Information</h4>
                                        <div class="row">
                                            <div class="prosonal-info">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>First Name:- </span> {{$customer->fname}}</p>
                                                    <p><span>Last Name:- </span> {{$customer->lname}}</p>
                                                    <p><span>Email Address:- </span> {{$customer->email}}</p>
                                                    <p><span>User Id:- </span> {{$customer->user_id}}</p>
                                                    <p><span>Phone Number:- </span> {{$customer->phone}}</p>
                                                    <p><span>Alt Number:- </span> {{$customer->alternate_phone}}</p>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>Address:- </span>  {{$customer->address}}</p>


                                                    <form action="{{route('admin.customer.changepwd')}}"
                                                    method="post"
                                                    >

                                                        @csrf


                                                        <input type="hidden" name="id" value="{{$customer->id}}">

                                                        <input class="form-control" type="text" name="password" value=""
                                                               placeholder="Password">

                                                        <button type="submit">UpdatePassword</button>


                                                    </form>


                                                    <a href="{{route('admin.customer.changepwdemail',$customer->id)}}">
                                                        Password Change By Email
                                                    </a>



                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">


                    <table class="table table-striped table-bordered" id="user-table" style="width: 100%">
                        <thead class="thead-dark">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Status</th>

                            <th>Issue</th>

                            <th>Resolved By</th>
                            <th>secure_id</th>
                            <th>alt_contact</th>
                            <th>Ticket Issued</th>
                            <th>Ticket Solved</th>



                        </tr>
                        </thead>
                    </table>


                </div>
            </div>

        </div>

    </div>
        </div>
    </div>
    <!-- End Contain Section -->

    @endsection


@section('footer')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>


    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>


    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.support.user.api',$customer->id)}}',
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'customer_id', name: 'customer_id' },
                    { data: 'phone', name: 'phone' },


                    { data: 'medium', name: 'medium' },
                    { data: 'result', name: 'result' },
                    { data: 'closed_by', name: 'closed_by' },
                    { data: 'secure_id', name: 'secure_id' },
                    { data: 'alt_contact', name: 'alt_contact' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },

                ]
            });

        });
    </script>




@stop
