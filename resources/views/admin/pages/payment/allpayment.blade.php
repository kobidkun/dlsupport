@extends('admin.base')
@section('content')

    <div class="container-fluid right_color">
        <div class="page-main-header">

                          <style>
                    #customers {
                        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }

                    #customers td, #customers th {
                        border: 1px solid #ddd;
                        padding: 8px;
                    }

                    #customers tr:nth-child(even){background-color: #f2f2f2;}

                    #customers tr:hover {background-color: #ddd;}

                    #customers th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                    }
                </style>



            <table class="table table-striped table-bordered" id="user-table" style="width: 100%">
                <thead class="thead-dark">
                <tr>
                    <th>fname</th>
                    <th>lname</th>
                    <th>email</th>
                    <th>phone</th>
                    <th>user_id</th>
                    <th>status</th>
                    <th>txtid</th>
                    <th>plan</th>
                    <th>ACTION</th>



                </tr>
                </thead>
            </table>



        </div>
    </div>
    @endsection



@section('footer')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>




    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.payment.api')}}',
                buttons: [
                    'copy', 'excel', 'pdf'
                ],
                columns: [
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'status', name: 'status' },
                    { data: 'txtid', name: 'txtid' },
                    { data: 'plan', name: 'plan' },

                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>




@stop