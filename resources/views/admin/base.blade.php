<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="bootstrap default admin template">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{env('APP_NAME')}} </title>


    <!-- Start global css -->
    <link rel="stylesheet" href="{{asset('css/waves.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <!-- End global css -->

    <!-- Start page plugin css -->
    <link rel="stylesheet" href="{{asset('css/jqvmap.css')}}">
    <!-- End page plugin css -->

    <!-- Start template global css -->
    <link href="{{asset('css/components.min.css')}}" type="text/css" rel="stylesheet"/>
    <!-- End template global css -->

    <!-- Start layout css -->
    <link rel="stylesheet" href="{{asset('css/left_menu_layout.min.css')}}"/>
    <!-- End layout css -->

    <!-- Start favicon ico -->
    <link rel="icon" href="https://www.dreamlink.in/images/fab.png" type="image/x-icon"/>
    <link rel="icon" type="image/png" sizes="192x192" href="https://www.dreamlink.in/images/fab.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://www.dreamlink.in/images/fab.png">
    <!-- End favicon ico -->

</head>
<body class="nav-medium">

<div class="container body">
    <div class="main_container">



        <!-- Start Loader -->
        {{--<div class="page-loader">--}}
            {{--<div class="preloader loading">--}}
                {{--<span class="slice"></span>--}}
                {{--<span class="slice"></span>--}}
                {{--<span class="slice"></span>--}}
                {{--<span class="slice"></span>--}}
                {{--<span class="slice"></span>--}}
                {{--<span class="slice"></span>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- End Loader -->

        <!-- Start Scroll Top -->
        <a href="javascript:" id="scroll" style="display: none;"><span></span></a>
        <!-- End Scroll Top -->



   @include('admin.component.side-bar')

    @include('admin.component.header')

    @yield('content')


    @include('admin.component.footer')















<!-- Start core js -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/waves.min.js')}}"></script>
<script src="{{asset('js/screenfull.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>
<!-- Start core javascript -->

<!-- Start global js -->
<script src="{{asset('js/left-menu.min.js')}}"></script>
<!-- End global js -->

<!-- Start page plugin js -->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>--}}
<script src="{{asset('js/Chart.min.js')}}"></script>
<script src="{{asset('js/utils.js')}}"></script>
<script src="{{asset('js/jquery.sparkline.js')}}"></script>
<script src="{{asset('js/jquery.vmap.js')}}"></script>
<script src="{{asset('js/jquery.vmap.world.js')}}"></script>
<script src="{{asset('js/jquery.vmap.sampledata.js')}}"></script>
<!-- End page plugin js -->

<!-- Start page js -->
<script src="{{asset('js/dashboard_v1.min.js')}}"></script>
<!-- End page js -->

     @yield('footer')



</body>
</html>
