@extends('admin.base')
@section('content')
    <div class="container-fluid right_color">
        <div class="page-main-header">

    <!-- Start Contain Section -->
    <div class="">


        <div class="contain-section">
            <div class="profile-background">

                <div class="profile-social">
                    <ul>
                        <li class="profile-name">
                            <h4>{{$details->user_id}} </h4>
                            <span><i class="fa fa-folder"></i>
                                {{$details->plan}}</span>
                        </li>
                        <li class="profile-follow">
                            <h6>{{$details->status}}</h6>
                            <span>status</span>
                        </li>
                        <li class="profile-follow">
                            <h6>{{$details->txtid}}</h6>
                            <span>txtid</span>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="profile-left-section">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="tab" href="#info" class="float-button-light">Information</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div id="info" class="tab-pane fade in active">
                                <div class="section-body">
                                    <div class="profile-body">
                                        <h4 class="profile-title"><i class="fa fa-hand-o-right"></i> Personal Information</h4>
                                        <div class="row">
                                            <div class="prosonal-info">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>First Name:- </span> {{$details->create_customers->fname}}</p>
                                                    <p><span>Last Name:- </span> {{$details->create_customers->lname}}</p>
                                                    <p><span>Email Address:- </span> {{$details->create_customers->email}}</p>
                                                    <p><span>User Id:- </span> {{$details->create_customers->user_id}}</p>
                                                    <p><span>Phone Number:- </span> {{$details->create_customers->phone}}</p>
                                                    <p><span>Alt Number:- </span> {{$details->create_customers->alternate_phone}}</p>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>Address:- </span>  {{$details->create_customers->address}}</p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
        </div>
    </div>
    <!-- End Contain Section -->

    @endsection
