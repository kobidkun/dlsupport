<?php

namespace App\Model\Referal;

use Illuminate\Database\Eloquent\Model;

class CreateReferal extends Model
{
    public function create_customers()
    {
        return $this->belongsTo('App\Model\Customer\Customer','id','customer_id');
    }
}
