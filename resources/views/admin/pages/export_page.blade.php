@extends('admin.base')
@section('content')
    <div class="container-fluid right_color">
        <div class="page-main-header">
            <div style="padding: 60px 200px 60px 300px;">
                <div class="section-header">
                    <h2>Export Form</h2>
                </div>
                <div class="section-body">
                    <form class="form-inline" method="post" action="{{route('admin.support.export')}}">


                        @csrf


                        <div class="form-group form-material">
                            <label class="control-label">From</label>
                            <input type="date" class="form-control" name="from" placeholder="">
                        </div>
                        <div class="form-group form-material">
                            <label class="control-label">To</label>
                            <input type="date" class="form-control" name="to" placeholder="">
                        </div>
                        <label class="control control-checkbox control-inline">Remember me
                            <input type="checkbox" checked="checked">
                            <span class="control-indicator"></span>
                        </label>

                        <button type="submit"
                                class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">Submit</button>
                    </form>

                </div>
            </div>
            <!------ Include the above in your HEAD tag ---------->


        </div>
    </div>

    @endsection
