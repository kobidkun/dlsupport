<?php

namespace App\Http\Controllers\Admin\Payment;

use App\Model\Customer\Customer;
use App\Model\Payment\CreatePayment ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManagePayment extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    public function CreatePayment(){
        return view('admin.pages.payment.create_payment');
    }

    public function CreatePaymentSave(Request $request){

        $customer = Customer::find($request->customer_id);

        $c = new CreatePayment();
        $c->customer_id = $request->customer_id;
        $c->amount = $request->amount;
        $c->status = 'pending';
        $c->plan = $request->plan;
        $c->user_id = $customer->user_id;
        $c->phone = $customer->phone;
        $c->email = $customer->email;
        $c->lname = $customer->lname;
        $c->fname = $customer->fname;
        $c->txtid = $this->GenerateRandom();

        $c->save();

        return redirect()->route('admin.payment.details',$c->id);

    }


    public function AllPayment(){
        return view('admin.pages.payment.allpayment');
    }



    public function AllPaymentAPI(){

        $customers = CreatePayment::select([
            'id',
            'fname',
            'lname',
            'email',
            'phone',
            'user_id',
            'status',
            'txtid',
            'plan'
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)
            ->addColumn('action', function ($invoice) {
                return '<a href="' . route('admin.payment.details', $invoice->id) . '" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->make(true);
    }



    public function Details($id){

        $customer = CreatePayment::find($id);

        return view('admin.pages.payment.viewdetails')->with([
            'details' => $customer
        ]);
    }




    public function GetAllPaymentviaSearch(Request $request){






        $query = $request->get('term','');

        $customers = CreatePayment::where('fname', 'LIKE', '%' . $query . '%')
            ->orwhere('user_id', 'LIKE', '%' . $query . '%')

            ->take(5)->get();

        $data=array();
        foreach ($customers as $customer) {
            $data[]=array(
                'value'=>$customer->user_id.' | '.$customer->fname.' | '.$customer->lname,
                'id'=>$customer->id,
                'fname'=>$customer->fname,
                'lname'=>$customer->lname

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }


    public function GenerateRandom(){
        $rand = rand(200000,1000000);
        $date = time();

        return $rand.$date;

    }

}
