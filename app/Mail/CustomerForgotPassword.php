<?php

namespace App\Mail;

use App\Model\Customer\CustomerPasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerForgotPassword extends Mailable
{
    use Queueable, SerializesModels;



    protected $passwordreset;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CustomerPasswordReset $passwordreset)
    {
        $this->passwordreset = $passwordreset;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.customerpasswordreset')->with([
            'url' => $this->passwordreset->slug,

        ]);
    }
}
