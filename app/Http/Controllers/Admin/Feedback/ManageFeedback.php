<?php

namespace App\Http\Controllers\Admin\Feedback;

use App\Model\Customer\Customer;
use App\Model\Feedback\CreateFeedBack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageFeedback extends Controller
{




    public function __construct()
    {
        $this->middleware('auth:admin');
    }








    public function AllFeedback(){
        return view('admin.pages.feedback.allfeedback');
    }



    public function AllFeedbackAPI(){

        $customers = CreateFeedBack::
        where('received', 'true')
            ->select([
            'id',
            'customer_id',
            'support_id',
            'star',
            'title',
            'description'
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)
            ->editColumn('customer_id', function ($invoice) {
               $cust = Customer::find($invoice->customer_id);
                return $cust->fname.' '.$cust->lname.' | '.$cust->user_id ;
            })


            ->rawColumns(['action'])
            ->make(true);
    }



    public function Details($id){

        $customer = CreateFeedBack::find($id);

        return view('admin.pages.feedback.editfeedback')->with([
            'details' => $customer
        ]);
    }






}



