<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('fname')->nullable();
            $table->text('lname')->nullable();
            $table->text('address')->nullable();
            $table->string('user_id')->nullable();
            $table->string('email')->unique();
            $table->text('phone');
            $table->text('plan')->nullable();
            $table->text('plan_value')->nullable();
            $table->text('alternate_phone')->nullable();
            $table->text('secure')->nullable();
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
