@extends('admin.base')
@section('content')

    <div class="container-fluid right_color">
        <div class="page-main-header">

            <div style="padding: 60px 200px 60px 300px;" >
                <div class="section-header">
                    <h2>Create Staff</h2>
                </div>
                <div class="section-body">
                    <label>Name</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" name="name" value="" placeholder="Name">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        </div>
                    </div>
                    <label>Email</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="email" name="email" value="" placeholder="email">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                    <label>Password</label>
                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control" type="text" name="pass" value="" placeholder="password">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                        </div>
                    </div>










                    <button type="button" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">Submit</button>
                    <button type="button" class="btn btn-danger btn-outline float-button-light waves-effect waves-button waves-float waves-light">Reset</button>

                </div>
            </div>

            <!------ Include the above in your HEAD tag ---------->


        </div>
    </div>


@endsection
