<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_supports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('customer_id')->nullable();
            $table->text('title')->nullable();
            $table->text('secure_id');
            $table->text('body')->nullable();
            $table->text('assigned_to')->nullable();
            $table->text('status')->nullable();
            $table->text('medium')->nullable();
            $table->text('closed_by')->nullable();
            $table->text('alt_contact')->nullable();
            $table->text('result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_supports');
    }
}
