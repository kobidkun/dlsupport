<?php

namespace App\Model\Customer\Support;

use Illuminate\Database\Eloquent\Model;

class CreateSupport extends Model
{
    public function create_feed_backs()
    {
        return $this->hasOne('App\Model\Feedback\CreateFeedBack','support_id','id');
    }

    public function customers()
    {
        return $this->hasOne('App\Model\Customer\Customer','id','customer_id');
    }
}
