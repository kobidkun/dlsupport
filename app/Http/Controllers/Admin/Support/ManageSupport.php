<?php

namespace App\Http\Controllers\Admin\Support;

use App\Model\Customer\Customer;
use App\Model\Customer\Support\CreateSupport;
use App\Model\Feedback\CreateFeedBack;
use App\Model\Staff\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;


class ManageSupport extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function alldumpcust(){
      //  $a = Customer::all();

        $a = 'jjjj';
        return response()->json($a);
    }


    public function CreateSupport(){
        return view('admin.pages.support.create_support');
    }

    public function CreateSupportSave(Request $request){

        $c = new CreateSupport();
        $c->customer_id = $request->customer_id;
        $c->title = $request->title;
        $c->body = $request->body;
        $c->status = 'open';
        $c->medium = 'Web';
        $c->secure_id = $this->GenerateRandom();
        $c->alt_contact = $request->alt_contact;
        $c->updated_at = null;
        $c->assigned_to = $this->AsignStaff();

        $c->save();

        return redirect()->route('admin.support.details',$c->id);

    }





    public function AllSupport(){
        return view('admin.pages.support.allsupport');
    }

    public function GenerateRandom(){
        $rand = rand(200000,1000000);
        $date = time();

        return $rand.$date;

    }

    public function AllSupportAPI(){

        $customers = CreateSupport::select([
            'id',
            'customer_id',

            'assigned_to',
            'status',
            'medium',
            'result',
            'closed_by',
            'alt_contact',
            'created_at',
            'secure_id'
        ])->orderBy('created_at', 'DESC');

        return DataTables::of($customers)
            ->addColumn('action', function ($invoice) {
                return '<a href="' . route('admin.support.details', $invoice->id) . '" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })

            ->editColumn('customer_id', function ($invoice) {

                $cs = Customer::find($invoice->customer_id);

                return $cs->fname.' '.$cs->lname;
            })

            ->editColumn('status', function ($invoice) {

                if ($invoice->status === 'open'){
                    return '<p style="background-color: red; color: #ffffff">Open</p>';
                }

                if ($invoice->status === 'Sent To Technical Team'){
                    return '<p style="background-color: yellow">Sent To Technical Team</p>';
                }

                if ($invoice->status === 'Complete'){
                    return '<p style="background-color: green; color: #ffffff">Complete</p>';
                }

                if ($invoice->status === 'Pending'){
                    return '<p style="background-color: yellow">Pending</p>';
                }


            })


            ->addColumn('phone', function ($invoice) {

                $cs = Customer::find($invoice->customer_id);

                return $cs->phone;
            })


            ->rawColumns(['action','status'])
            ->make(true);
    }



    public function AllSupportAPIUser($id){

        $customers = CreateSupport::where('customer_id', $id)->select([
            'id',
            'customer_id',
            'assigned_to',
            'status',
            'medium',
            'result',
            'closed_by',
            'alt_contact',
            'secure_id',
            'created_at',
            'updated_at'
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)
            ->addColumn('action', function ($invoice) {
                return '<a href="' . route('admin.support.details', $invoice->id) . '" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })

            ->editColumn('customer_id', function ($invoice) {

                $cs = Customer::find($invoice->customer_id);

                return $cs->fname.' '.$cs->lname;
            })

            ->editColumn('updated_at', function ($invoice) {

                if ($invoice->updated_at !== null ){
                    return  $invoice->updated_at->toDayDateTimeString();
                } else  {
                    return " Unsolved";
                }


            })

            ->editColumn('created_at', function ($invoice) {

              return  $invoice->created_at->toDayDateTimeString();
            })

            ->editColumn('status', function ($invoice) {

                if ($invoice->status === 'open'){
                    return '<p style="background-color: red; color: #ffffff">Open</p>';
                }

                if ($invoice->status === 'Sent To Technical Team'){
                    return '<p style="background-color: yellow">Sent To Technical Team</p>';
                }

                if ($invoice->status === 'Complete'){
                    return '<p style="background-color: green; color: #ffffff">Complete</p>';
                }

                if ($invoice->status === 'Pending'){
                    return '<p style="background-color: yellow">Pending</p>';
                }


            })


            ->addColumn('phone', function ($invoice) {

                $cs = Customer::find($invoice->customer_id);

                return $cs->phone;
            })


            ->rawColumns(['action','status'])
            ->make(true);

    }

    public function Details($id){

        $support = CreateSupport::find($id);
        $customer = Customer::find($support->customer_id);

        return view('admin.pages.support.viewdetails')->with([
            'support' => $support,
            'customer' => $customer,
        ]);
    }


    public  function AsignStaff(){

        $staff = Staff::all()->random(1)->first();

        return $staff->id;

    }


    public function SentToTec($id){

        $support = CreateSupport::find($id);
        $support->status = 'Sent To Technical Team';
        $support->save();

        return back();


    }


    public function Complete($id){

        $support = CreateSupport::find($id);
        $support->status = 'Complete';
        $support->save();

       // $customerDetails = Customer::find($support->customer_id);

     //   $this->CreateFeedback($customerDetails, $support);



        return back();

    }

    public function UpdateResult(Request $request){
        $a  = CreateSupport::find($request->id);

        $customerDetails = Customer::where('id', $a->customer_id)->first();

        $a->result = $request->result;
        $a->closed_by = $request->closed_by;

        $a->save();

        $this->Complete($request->id);


        if ( $findfbk = CreateFeedBack::where('support_id',$request->id) === null){

            $a = new CreateFeedBack();

            $a->customer_id = $customerDetails->id;
            $a->support_id = $a->id;
            $a->secure = $this->GenerateRandom();
            $a->save();
        }





        return back();



    }


    public function Pending($id){

        $support = CreateSupport::find($id);
        $support->status = 'Pending';
        $support->save();

        return back();
    }

    public function CreateFeedback($customerDetails, $support){

       if ( $findfbk = CreateFeedBack::where('support_id',$support->id) === null){

           $a = new CreateFeedBack();

           $a->customer_id = $customerDetails->id;
           $a->support_id = $support->id;
           $a->secure = $this->GenerateRandom();
           $a->save();
       }




    }


    public function Dahboard(){
        $cust = Customer::all()->count();
        $supp = CreateSupport::all()->count();
        $feed = CreateFeedBack::all()->count();

        return view('admin.pages.dashboad.dashboard')->with([
            'cust' => $cust,
            'supp' => $supp,
            'feed' => $feed,


        ]);

    }

    public function export(Request $request)
    {

    $a =    CreateSupport::whereBetween(DB::raw('DATE(created_at)'), array($request->from, $request->to))->get();



        $csvExporter = new \Laracsv\Export();
        $users = $a;

// Register the hook before building


        $csvExporter->build($users, [
            'title',
            'customers.fname' => 'F NAME',
            'customers.lname' => 'L NAME',
            'customers.user_id' => 'user_id',
            'customers.email' => 'email',
            'customers.phone' => 'phone',
            'customers.plan' => 'plan',
            'status' => 'status',
            'closed_by' => 'closed_by',
            'alt_contact' => 'alt_contact',
            'updated_at' => 'closed at'
        ]);

        $csvExporter->download();

    }



}
