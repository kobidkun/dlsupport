@extends('admin.base')
@section('content')

    <div class="container-fluid right_color">
        <div class="page-main-header">
            <div style="padding: 60px 200px 60px 300px;">
                <div class="section-header">
                    <h2>Create Support</h2>
                </div>


                <form action="{{route('admin.payment.create')}}" method="post">

                    @csrf
                    <input type="hidden" name="customer_id" id="customer_id">


                    <div class="section-body">
                        <label>Customer Id</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" type="text" required name="" id="customer" value="" placeholder="Customer User Id">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                        </div>
                        <label>Title</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" type="text" required name="plan" value="" placeholder="Plan">
                                <span class="input-group-addon"><i class="fa fa-file"></i></span>
                            </div>
                        </div>
                        <label>Amount</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" type="text" required name="amount" value="" placeholder="Amount">
                                <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                            </div>
                        </div>


                        <label>alt_contact</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control" type="text"  name="alt_contact" value="" placeholder="alt_contact">
                                <span class="input-group-addon"><i class="fa fa-file"></i></span>
                            </div>
                        </div>




                        <button type="submit" class="btn btn-primary btn-outline float-button-light waves-effect waves-button waves-float waves-light">Submit</button>
                        <button type="reset" class="btn btn-danger btn-outline float-button-light waves-effect waves-button waves-float waves-light">Reset</button>

                    </div>
                </form>


            </div>

            <!------ Include the above in your HEAD tag ---------->


        </div>
    </div>


@endsection


@section('footer')


    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.css"/>
    <link rel="stylesheet" href="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.theme.min.css"/>
    <script src="https://service.emporiummarketing.com/plugin/ui-autocomplete/jquery-ui.min.js" type="text/javascript"></script>


    <script>
        $(document).ready(function () {


            var src = '{{route('admin.customer.api.search')}}';
            $("#customer").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.fname+' '+ui.item.lname);
                    $('#customer_id').val(ui.item.id);





                }

            });
        });
    </script>

@stop
