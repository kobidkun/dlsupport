<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Mail\CustomerForgotPassword;

use App\Model\Customer\Customer;
use App\Model\Customer\CustomerPasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;


class ManageCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    public function CreateCustomer(){
        return view('admin.pages.customer.create_customer');
    }




    public function CreateCustomerupdate($id){

        $c = Customer::find($id);

        return view('admin.pages.customer.update')->with([
            'customer' => $c
        ]);
    }

    public function CreateCustomerSave(Request $request){

        $c = new Customer();
        $c->fname = $request->fname;
        $c->lname = $request->lname;
        $c->address = $request->address;
        $c->user_id = $request->user_id;
        $c->email = $request->email;
        $c->phone = $request->phone;
        $c->alternate_phone = $request->alternate_phone;
        $c->plan = $request->plan;
        $c->plan_value = $request->plan_value;
        $c->secure = $this->GenerateRandom();
        $c->password = bcrypt($request->password);
        $c->save();


        return redirect()->route('admin.customer.details',$c->id);

    }

    public function CreateCustomerUpdatesave(Request $request){

        $c = Customer::find($request->id);
        $c->fname = $request->fname;
        $c->lname = $request->lname;
        $c->address = $request->address;
        $c->user_id = $request->user_id;
        $c->email = $request->email;
        $c->phone = $request->phone;
        $c->alternate_phone = $request->alternate_phone;
        $c->plan = $request->plan;
        $c->plan_value = $request->plan_value;


        $c->save();


        return redirect()->route('admin.customer.details',$c->id);

    }


    public function AllCustomer(){
        return view('admin.pages.customer.allcustomer');
    }

    public function GenerateRandom(){
        $rand = rand(200000,1000000);
        $date = time();

        return $rand.$date;

    }

    public function AllCustomerAPI(){

        $customers = Customer::select([
            'id',
            'fname',
            'lname',
            'phone',
            'user_id',
            'alternate_phone',

        ])->orderBy('created_at', 'DESC');

        return DataTables::of($customers)
            ->addColumn('action', function ($invoice) {
                return '<a href="' . route('admin.customer.details', $invoice->id) . '" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->make(true);
    }



    public function Details($id){

        $customer = Customer::find($id);

        return view('admin.pages.customer.viewdetails')->with([
            'customer' => $customer
        ]);
    }




    public function GetAllCustomerviaSearch(Request $request){






        $query = $request->get('term','');

        $customers = Customer::where('fname', 'LIKE', '%' . $query . '%')
            ->orwhere('user_id', 'LIKE', '%' . $query . '%')

            ->take(5)->get();

        $data=array();
        foreach ($customers as $customer) {
            $data[]=array(
                'value'=>$customer->user_id.' | '.$customer->fname.' | '.$customer->lname,
                'id'=>$customer->id,
                'fname'=>$customer->fname,
                'lname'=>$customer->lname

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }


    public function ChangePassword(Request $request){

        $pass = Customer::find($request->id);

        $pass->password = bcrypt($request->password);

        $pass->save();


        return back();



    }


    public function PasswordRequestByMail($id){
        $pp = Customer::find($id);


        if ($pp->password_resets === null){
            $bb = new  CustomerPasswordReset();
            $bb->customer_id = $pp->id;
            $bb->slug = time();
            $bb->save();

            Mail::to($pp->email)->send(new  CustomerForgotPassword($bb));

        } else {

            $pwr = $pp->password_resets;

            Mail::to($pp->email)->send(new  CustomerForgotPassword($pwr));



        }




        return redirect()->route('admin.customer.create');

}




}
