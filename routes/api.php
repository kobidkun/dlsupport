<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/admin/customer/seen', 'Admin\Support\ManageSupport@alldumpcust');


Route::post('customer/register', 'Customer\RegisterCustomer@RegisternewCustomer');
Route::post('customer/password', 'Customer\RegisterCustomer@ForgotPasswordRequest');
Route::get('customer/profile', 'Customer\ManageCustomer@ReturnProfile');



Route::post('customer/create/support', 'Customer\CreateSupportRequestController@CreateSupportRequest');
Route::post('customer/support/upload/image', 'Customer\CreateSupportRequestController@UploadImage');
Route::get('customer/get/support', 'Customer\CreateSupportRequestController@GetSupportRequest');


Route::get('customer/create/reply', 'Customer\RegisterCustomer@RegisternewCustomer');
Route::get('customer/create/upload/files', 'Customer\RegisterCustomer@RegisternewCustomer');



Route::get('customer/payment/all', 'Customer\ManagePayment@AllPayments');
Route::get('customer/payment/success', 'Customer\ManagePayment@SuccessPayments');
Route::get('customer/payment/pending', 'Customer\ManagePayment@LatestPendingPayment');
Route::get('customer/payment/status/{slug}', 'Customer\ManagePayment@PaymentStatus');


Route::post('customer/feedback/get', 'Customer\ManageFeedback@RecieveFeedback');

Route::post('customer/password/change', 'Customer\ManageCustomer@PasswordChange');