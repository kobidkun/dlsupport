@extends('admin.base')
@section('content')
    <div class="container-fluid right_color">
        <div class="page-main-header">

    <!-- Start Contain Section -->
    <div class="">


        <div class="contain-section">

            <div class="row">
                <div class="col-md-12">
                    <div class="profile-left-section">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="tab" href="#info" class="float-button-light">Support Information</a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div id="info" class="tab-pane fade in active">
                                <div class="section-body">
                                    <div class="profile-body">
                                        <h4 class="profile-title"><i class="fa fa-hand-o-right"></i> Support Information</h4>
                                        <div class="row">
                                            <div class="prosonal-info">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <p><span>First Name:- </span> {{$customer->fname}}</p>
                                                    <p><span>Last Name:- </span> {{$customer->lname}}</p>
                                                    <p><span>Email Address:- </span> {{$customer->email}}</p>
                                                    <p><span>User Id:- </span> {{$customer->user_id}}</p>
                                                    <p><span>Phone Number:- </span> {{$customer->phone}}</p>
                                                    <p><span>Alt Number:- </span> {{$customer->alternate_phone}}</p>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">


                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <p><span>secure id:- </span>##{{$support->secure_id}}</p>

                                                        <p><span>Description:- </span>  {{$support->body}}</p>
                                                        <p><span>status:- </span>  {{$support->status}}</p>
                                                        <p><span>Issue:- </span>  {{$support->result}}</p>
                                                        <p><span>alt_contact:- </span>  {{$support->alt_contact}}</p>
                                                        <p><span>Creation Time:- </span>  {{$support->created_at}}</p>
                                                        <p><span>Closed Time:- </span>  {{$support->updated_at}}</p>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>



                                        <div class="row">



                                         {{--   <div class="col-md-4">
                                                <a href="{{route('admin.support.stat.pending',$support->id)}}" class="btn btn-danger">Pending</a>
                                            </div>
                                            <div class="col-md-4" >
                                                <a href="{{route('admin.support.stat.complete',$support->id)}}" class="btn btn-success">Mark As Complete</a>
                                            </div>--}}


                                        </div>


                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>

                                        @if ($support->updated_at === null)



                                        <form action="{{route('admin.update.result')}}"
                                              method="post"

                                        >

                                            <input type="hidden" name="id" value="{{$support->id}}">
                                            <input type="text" required name="result" placeholder="Update Issue" class="form-control" >
                                            <input type="text" required name="closed_by" placeholder="Resolved By"
                                                   class="form-control" >


                                            <button class="btn btn-primary" type="submit">Submit</button>

                                            @csrf


                                        </form>

                                        @endif


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
        </div>
    </div>
    <!-- End Contain Section -->

    @endsection
