<?php

namespace App\Http\Controllers\Customer;

use App\Mail\CustomerForgotPassword;
use App\Model\Customer\Customer;
use App\Model\Customer\CustomerPasswordReset;
use Carbon\Traits\Date;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class RegisterCustomer extends Controller
{
   public function RegisternewCustomer(Request $request){

       $a = new Customer();
       $a->fname = $request->fname;
       $a->lname = $request->lname;
       $a->address = $request->address;
       $a->user_id = $request->user_id;
       $a->email = $request->email;
       $a->phone = $request->phone;
       $a->secure = $this->GenerateRandom();
       $a->password = bcrypt($request->password);
       $a->save();

       return response()->json([
           'msg' => 'successful'
       ],200);

   }

   public function GenerateRandom(){
       $rand = rand(200000,1000000);
       $date = time();

       return $rand.$date;

   }

   public function ForgotPasswordRequest(Request $request){

       $user = Customer::where('user_id', $request->user_id)
           ->orwhere('email', $request->email)->first();

       $this->PasswordRequestByMail($user->id);

       return response()->json(true,200);



   }


    public function PasswordRequestByMail($id)
    {
        $pp = Customer::find($id);


        if ($pp->password_resets === null) {
            $bb = new  CustomerPasswordReset();
            $bb->customer_id = $pp->id;
            $bb->slug = time();
            $bb->save();

            Mail::to($pp->email)->send(new  CustomerForgotPassword($bb));

        } else {

            $pwr = $pp->password_resets;

            Mail::to($pp->email)->send(new  CustomerForgotPassword($pwr));


        }

    }


    public function ResetPasswordCustomer($slug){
       $a = CustomerPasswordReset::where('slug','=',$slug)->first();

       //dd($a);

       $customer = Customer::where('id', $a->customer_id)->first();

       //dd($customer);

       return view('front.web.pages.password')
           ->with([
               'cust' => $customer
           ]);

    }


    public function UpdatePassword(Request $request){
       $u = Customer::find($request->id);

       $u->password = bcrypt($request->password);


           $u->save();


           return 'Your Password Has been Successfully Changed';










    }


}
