<?php

namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerPasswordReset extends Model
{
    public function password_resets()
    {
        return $this->hasOne('App\Model\Customer\Customer','id','customer_id');
    }
}
