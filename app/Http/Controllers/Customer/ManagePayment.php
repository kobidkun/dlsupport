<?php

namespace App\Http\Controllers\Customer;

use App\Model\Payment\CreatePayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagePayment extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:customer-api');

    }


    public function AllPayments(Request $request){
        $user =  $request->user();


        $payment = CreatePayment::orderBy('updated_at', 'DESC')->where('customer_id', $user->id)->get();

        return response()->json($payment);

    }


    public function SuccessPayments(Request $request){
        $user =  $request->user();

        $payment = CreatePayment::where('status', 'success')->where('customer_id', $user->id)->get();

        return response()->json($payment);

    }


    public function LatestPendingPayment(Request $request){
        $user =  $request->user();
        $payment = CreatePayment::orderBy('updated_at', 'DESC')->where(
            [
                ['status', '=', 'pending'],
                ['customer_id', '=', $user->id],
            ])->get();



        return response()->json($payment->first());
    }

    public function PaymentStatus($slug){

        $payment = CreatePayment::where('txtid', $slug)->first();



        return response()->json($payment);
    }






}
