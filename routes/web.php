<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect()->route('admin.login');
});

Auth::routes();

Route::get('/pay-now', 'PublicController\ManagePayment@PayNow')->name('pay');
Route::get('/pay-web/{slug}', 'PublicController\ManagePayment@CreateWebPayment')->name('pay.web');
Route::get('/pay-web-status', 'PublicController\ManagePayment@CapturePaymentSuccessWeb')->name('web.success');
Route::get('/pay-mob/{slug}', 'PublicController\ManagePayment@CreateMobPayment')->name('pay.mob');
Route::get('/pay-mob-status', 'PublicController\ManagePayment@CapturePaymentSuccessMobile')->name('pay.mob.status');
Route::get('/password-changee/{slug}', 'Customer\RegisterCustomer@ResetPasswordCustomer')->name('password.reset.view');
Route::post('/password-changee/update', 'Customer\RegisterCustomer@UpdatePassword')->name('password.reset.update');



Route::get('/home', 'HomeController@index')->name('home');



Route::get('/admin/login', 'Admin\Auth\AdminAuthController@loginview')
    ->name('admin.login');


Route::get('/admin/logout', 'Admin\Auth\AdminAuthController@logout')
    ->name('admin.logout');

Route::post('/admin/login', 'Admin\Auth\AdminAuthController@StoreLogin')
    ->name('admin.login.post');


Route::get('/admin/customer/create', 'Admin\Customer\ManageCustomer@CreateCustomer')
    ->name('admin.customer.create');

Route::post('/admin/customer/create', 'Admin\Customer\ManageCustomer@CreateCustomerSave')
    ->name('admin.customer.save');

Route::get('/admin/customer', 'Admin\Customer\ManageCustomer@AllCustomer')
    ->name('admin.customer');

Route::get('/admin/customer/api', 'Admin\Customer\ManageCustomer@AllCustomerAPI')
    ->name('admin.customer.api');

Route::get('/admin/customer/{id}', 'Admin\Customer\ManageCustomer@Details')
    ->name('admin.customer.details');

Route::get('/admin/customer/api/search', 'Admin\Customer\ManageCustomer@GetAllCustomerviaSearch')
    ->name('admin.customer.api.search');

Route::post('/admin/customer/changepwd', 'Admin\Customer\ManageCustomer@ChangePassword')
    ->name('admin.customer.changepwd');

Route::get('/admin/customer/changepwdemail34/{id}', 'Admin\Customer\ManageCustomer@PasswordRequestByMail')
    ->name('admin.customer.changepwdemail');


Route::post('/admin/customer/profile', 'Admin\Customer\ManageCustomer@CreateCustomerUpdatesave')
    ->name('admin.customer.profile');

Route::get('/admin/customer/profile/{id}', 'Admin\Customer\ManageCustomer@CreateCustomerupdate')
    ->name('admin.customer.profile.get');




// support


Route::get('/admin/dashboard', 'Admin\Support\ManageSupport@Dahboard')
    ->name('admin.dashboard.create');


Route::get('/admin/support/create', 'Admin\Support\ManageSupport@CreateSupport')
    ->name('admin.support.create');

Route::post('/admin/support/create', 'Admin\Support\ManageSupport@CreateSupportSave')
    ->name('admin.support.save');

Route::get('/admin/support', 'Admin\Support\ManageSupport@AllSupport')
    ->name('admin.support');

Route::post('/admin/support/updateresult', 'Admin\Support\ManageSupport@UpdateResult')
    ->name('admin.update.result');

Route::get('/admin/support/api', 'Admin\Support\ManageSupport@AllSupportAPI')
    ->name('admin.support.api');

Route::get('/admin/support/user/api/{id}', 'Admin\Support\ManageSupport@AllSupportAPIUser')
    ->name('admin.support.user.api');

Route::get('/admin/support/{id}', 'Admin\Support\ManageSupport@Details')
    ->name('admin.support.details');


Route::get('/admin/support/status/tech/{id}', 'Admin\Support\ManageSupport@SentToTec')
    ->name('admin.support.stat.tech');

Route::get('/admin/support/status/complete/{id}', 'Admin\Support\ManageSupport@Complete')
    ->name('admin.support.stat.complete');

Route::get('/admin/support/status/pending/{id}', 'Admin\Support\ManageSupport@Pending')
    ->name('admin.support.stat.pending');


Route::post('/admin/export/support', 'Admin\Support\ManageSupport@export')
    ->name('admin.support.export');




// payment


Route::get('/admin/payment/create', 'Admin\Payment\ManagePayment@CreatePayment')
    ->name('admin.payment.create');

Route::post('/admin/payment/create', 'Admin\Payment\ManagePayment@CreatePaymentSave')
    ->name('admin.payment.save');

Route::get('/admin/payment', 'Admin\Payment\ManagePayment@AllPayment')
    ->name('admin.payment');

Route::get('/admin/payment/api', 'Admin\Payment\ManagePayment@AllPaymentAPI')
    ->name('admin.payment.api');

Route::get('/admin/payment/{id}', 'Admin\Payment\ManagePayment@Details')
    ->name('admin.payment.details');

Route::get('/admin/payment/api/search', 'Admin\Payment\ManagePayment@GetAllPaymentviaSearch')
    ->name('admin.payment.api.search');



Route::get('/admin/feedback', 'Admin\Feedback\ManageFeedback@AllFeedback')
    ->name('admin.feedback');

Route::get('/admin/feedback/api', 'Admin\Feedback\ManageFeedback@AllFeedbackAPI')
    ->name('admin.feedback.api');

Route::get('/admin/feedback/{id}', 'Admin\Feedback\ManageFeedback@Details')
    ->name('admin.feedback.details');

//

Route::get('create_customer', function () {
    return view('admin.pages.create_customer');
});
Route::get('allcustomer', function () {
    return view('admin.pages.allcustomer');
});
Route::get('create_stuff', function () {
    return view('admin.pages.create_stuff');
});
Route::get('allstuff', function () {
    return view('admin.pages.allstuff');
});
Route::get('create_support', function () {
    return view('admin.pages.create_support');
});
Route::get('allsupport', function () {
    return view('admin.pages.allsupport');
});
Route::get('allfeedback', function () {
    return view('admin.pages.allfeedback');
});
Route::get('create_feedback', function () {
    return view('admin.pages.create_feedback');
});
Route::get('export_page', function () {
    return view('admin.pages.export_page');
});
Route::get('viewdetails', function () {
    return view('admin.pages.viewdetails');
});
