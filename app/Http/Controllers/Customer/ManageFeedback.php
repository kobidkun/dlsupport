<?php

namespace App\Http\Controllers\Customer;

use App\Model\Feedback\CreateFeedBack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageFeedback extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:customer-api');

    }

    public function RecieveFeedback(Request $request){

        $a = CreateFeedBack::where('secure' , $request->slug)->first();
        $a->star = $request->star;
        $a->title = $request->title;
        $a->description = $request->description;
        $a->received = 'true';

        $a->save();

        return response()->json([
            'msg' => 'successful'
        ],200);

    }


}
