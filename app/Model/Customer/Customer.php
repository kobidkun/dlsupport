<?php

namespace App\Model\Customer;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $fillable = [
        'name', 'email', 'password','user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */



    protected $hidden = [
        'password', 'remember_token',
    ];




    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('user_id', $identifier)->first();
    }


    public function create_supports()
    {
        return $this->hasMany('App\Model\Customer\Support\CreateSupport','customer_id','id');
    }


    public function password_resets()
    {
        return $this->hasOne('App\Model\Customer\CustomerPasswordReset','customer_id','id');
    }




}
