
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="bootstrap default admin template">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- START GLOBAL CSS -->
    <link href="{{asset('assets/global/plugins/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css" rel="stylesheet"/>
    <link href="{{asset('assets/global/plugins/Waves/dist/waves.min.css')}}" type="text/css" rel="stylesheet"/>
    <!-- END GLOBAL CSS -->

    <!-- START PAGE PLUG-IN CSS -->
    <link rel="stylesheet" href="{{asset('assets/icons_fonts/font-awesome/css/font-awesome.min.css')}}"/>
    <!-- END PAGE PLUG-IN CSS -->

    <!-- START TEMPLATE GLOBAL CSS -->
    <link href="{{asset('assets/pages/login/css/user_login_v1.css')}}" type="text/css" rel="stylesheet"/>
    <!-- END TEMPLATE GLOBAL CSS -->

    <!-- Start favicon ico -->
    <link rel="icon" href="{{asset('/images/fab.png')}}" type="image/x-icon"/>
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('/images/fab.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/fab.png')}}">
    <!-- End favicon ico -->

    <!-- Start favicon ico -->
    <link rel="icon" href="{{asset('/images/fab.png')}}" type="image/x-icon"/>
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('/images/fab.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/fab.png')}}">
    <!-- End favicon ico -->

</head>
<body>

<div class="login-background">
    <!--  START LOGIN -->
    <div class="login-page">
        <div class="main-login-contain">
            <div class="login-form">
                <img src="{{asset('/images/logo.png')}}" alt="logo-image">
                <h2>Welcome to the Dreamlink</h2>
                <div class="login-me-bottom">
                    <div class="goto-login">

                        <button type="submit" onclick="openNav()" class="btn btn-login float-button-light">Login now</button>
                    </div>
                </div>

                <div id="myNav" class="overlay">

                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

                    <div class="login-overlay-form">
                        <div class="main-login-contain">
                            <form id="form-validation" method="post"

                            action="{{route('admin.login.post')}}"

                            >

                                @csrf
                                <h4>Login</h4>

                                <div class="form-group">
                                    <input required="required" type="email" name="email" id="input-email">
                                    <label class="control-label" for="input-email">Enter Email</label>
                                    <i class="bar"></i>
                                </div>
                                <div class="form-group">
                                    <input required="required" name="password" type="password" id="input-password">
                                    <label class="control-label"  for="input-password">Enter Password</label><i class="bar"></i>
                                </div>

                                <div class="goto-login">
                                    <div class="forgot-password-login">
                                        <a href="#">
                                            <i class="icon icon_lock"></i> Forgot password?
                                        </a>
                                    </div>
                                    <button type="submit" class="btn btn-login float-button-light">Login</button>
                                </div>




                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  END LOGIN -->
</div>

<!-- START CORE JAVASCRIPT -->
<script src="{{asset('assets/global/plugins/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/global/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('../../assets/global/plugins/Waves/dist/waves.min.js')}}"></script>
<!-- END CORE JAVASCRIPT -->

<!-- START PAGE JAVASCRIPT -->
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }

    !(function ($) {
        if (typeof Waves !== 'undefined') {
            Waves.attach('.float-button-light', ['waves-button', 'waves-float', 'waves-light']);
            Waves.init();
        }
    })(jQuery);
</script>
<!-- END PAGE JAVASCRIPT -->

</body>
</html>