<?php

namespace App\Model\Feedback;

use Illuminate\Database\Eloquent\Model;

class CreateFeedBack extends Model
{
    public function create_customers()
    {
        return $this->belongsTo('App\Model\Customer\Customer','id','customer_id');
    }
}
